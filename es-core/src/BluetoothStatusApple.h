//  SPDX-License-Identifier: MIT
//
//  ES-DE Frontend
//  BluetoothStatusApple.h
//
//  Gets the Bluetooth adapter status on macOS.
//

#ifndef ES_CORE_BLUETOOTH_STATUS_APPLE_H
#define ES_CORE_BLUETOOTH_STATUS_APPLE_H

#ifdef __cplusplus
extern "C" {
int getBluetoothStatus();
}
#endif

#endif // ES_CORE_BLUETOOTH_STATUS_APPLE_H
